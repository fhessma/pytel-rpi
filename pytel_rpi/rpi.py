# rpi.py

"""
pytel module for operating a Raspberry Pi as a general-purpose controller
with GPIO I/O and a serial connection.

A typical configuration file might be something like

	serial :
		device : /dev/ttyUSB0
		baudrate : 9600
		bits : 8
		parity : N
		stopbit : 1
	roof :
		gpio :
			pins  : 17,19,20
			duration : 5
	watchdog :
		gpio :
			pins  : 21
			duration : 10
	humidity :
		gpio :
			pins  : 22
"""

import logging
import sys

from pytel.modules.module     import PytelModule
from pytel.interfaces.IStatus import IStatus
from pytel.interfaces.IAbort  import IAbort

from pytel_rpi.serialio       import RPiSerial
from pytel_rpi.ttlio          import RPiIO

log = logging.getLogger('pytel')


class RaspberryPi (PytelModule, RPiSerial, RPiIO, IStatus, IAbort):
	def __init__(self, *args, **kwargs):
		# initialize the pytel stuff
		PytelModule.__init__(self, *args, **kwargs)

		# intialize the GPIO stuff
		RPiIO.__init__(self, *args, **kwargs)

		# initialize the serial connection if needed
		if 'serial' in self.config :
			serial_info = self.config['serial']
			RPiSerial.__init__(self, serial_info, *args, **kwargs)

		# configure clients
		self._clients = []
		for client in self.config['clients'] :

			# configuration dictionary for this client
			client_conf = self.config['clients'][client]

			# default I/O configuration for any client
			gpio_conf = RPiIO.DEFAULT_CONFIG
			pin_conf = {}

			# for clients that use GPIO
			if 'gpio' in client_conf :
				info = client_conf['gpio']
				for thing in info :

					# note what is happening with which pin
					if thing == 'pins' :
						# convert string to list of int's
						try :
							gpio_conf['pins'] = [int(s) for s in info['pins'].split(',')
							for pin in gpio_conf['pins'] :
								pin_conf[pin] = gpio_conf
						except :
							log.error ('Unable to parse GPIO pins {0}'.format(info[thing]))
					elif thing in gpio_conf :
						gpio_conf[thing] = info[thing]
					else :
						log.error ('Unknown GPIO configuration key : {0}'.format(thing))

				# if pins are used, initialize
				if len(pin_conf) > 0 :
					for pin,d in pin_conf.items() :
						self.add_pin (pin,gpio_conf)

			# add finished client info
			self._clients.append({'client':None,'role':client,'gpio':gpio_conf})

	def close(self):
		if self._serial is not None :
			log.info('Closing serial connection...')
			self._serial.close()
		log.info('Clearing I/O ...')
		self.gpio_init()

	def status (self, *args, **kwargs) -> dict:
		""" IStatus report """
		return {**self._serial_status(), **self._gpio_status()}

	def register (self, role, client, *args, **kwargs) -> bool:
		"""
		Method for registering clients for serial and GPIO I/O.
		Each client has a role string (e.g. "watchdog") and access rights defined
			in the configuration YAML file.
		"""
		if role not in self._clients :
			log.error ('Registration attempted for unknown client role "{0}"'.format(role))
			return False

		self._clients[role]['client'] = client
		return True

	def _client_check (self, role,client) -> bool:
		""" Says whether the client is that registered for a particular role. """
		if role not in self._clients :
			log.error ('Unknown GPIO client role "{0}"'.format(role))
			return False
		info = self._clients[role]
		if client != info['client'] :
			log.error ('Wrong GPIO client for role "{0}"'.format(role))
			return False
		return True

	def set_gpio (self, role,client,vals) -> bool:
		""" Sets the GPIO pins appropriate for the client's role. """

		# check for right client
		if not self._client_check(role,client) :
			return False

		# get info and check that pins and values match
		info  = self._clients[role]
		pins  = info['pins']
		timer = info['timer']
		npins = len(pins)
		if len(vals) != npins :
	`		log.error ('GPIO values do not match number of pins {0}:{1}'.format(vals,pins))
			return False

		with self._gpio_lock :
			self._setpins (pins,vals)
			for i in range(npins) :
				info = self._gpio_pins[pin]
				info['val'] = vals[i]
				info['count'] = timer
		return True

