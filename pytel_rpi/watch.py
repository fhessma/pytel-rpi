# pytel-rpi.pytel_rpi.watch.py

"""
A network watchdog.
"""

import logging
import os
import sys
import threading

from enum import Enum

from pytel.interfaces.IStatus import IStatus

log = logging.getLogger ('pytel')

class NetworkWatchdog (IStatus) :
	"""
	Pings a list of URI's and tells the RPI GPIO object to set a watchdog output signal if
	enough of the URI's are reachable.
	"""
	class Status(Enum) :
		ERROR      = 'error'
		NO_NETWORK = 'no network'
		PINGING    = 'pinging'
		UNKNOWN    = 'unknown'

	def __init__ (self, conf, *args, **kwargs) :
		""" Initialize using a given time [secs] and a list of URI's. """

		# DEFAULTS
		self._pin        = 23
		self._timer      = 60.
		self._minping    = 1
		self._waits      = 3
		self._duration   = 5.
		self._ping_count = 0
		self._status     = NetworkWatchdog.Status.UNKNOWN

		# NECESSARY CONFIGURATION

		if 'cmd' not in conf :
			log.error ('ping "cmd" not in watchdog configuration dictionary!')
			raise KeyError('missing keyword "cmd"')
		else :
			self._ping_cmd = conf['cmd']

		if 'uris' not in conf :
			log.error ('"uris" not in watchdog configuration dictionary!')
			raise KeyError('missing keyword "uris"')
		self._uris = conf['uris']		# LIST OF URI STRINGS

		# OPTIONAL CONFIGURATION

		if 'timer' in conf :
			self._timer = conf['timer']		# SIGNAL REPEAT TIME IN SECONDS
		if 'min_pingable' in conf :
			self._minping = conf['min_pingable']
		if 'waits' in conf :
			self._waits = conf['waits']
		if 'duration' in conf :
			self._duration = conf['duration']	# LENGTH OF SIGNAL
		if 'pin' in conf :
			self._pin = conf['pin']

		# SET UP THREADS

		self._signaler = None
		self._watchdog_lock = threading.Lock()

		# START THE PINGING THREAD
		self._ping_thread = threading.Thread (target=self._start_pinging)
		self._ping_thread.setDaemon (True)
		self._ping_thread.start()

		# START THE SIGNALING THREAD
		self._signal_thread = threading.Thread (target=self._start_signaling)
		self._signal_thread.setDaemon (True)
		self._signal_thread.start()

	def _start_pinging (self,delay=1) :
		""" The thread defined by this method should perform the URI searches every "delay" seconds. """
		while True :
			self._ping ()
			time.sleep (delay)

	def _start_signaling (self,delay=45.) :
		""" The thread defined by this method should perform the URI searches every "delay" seconds. """
		while True :
			with self._watchdog_lock :
				if self._status == NetworkWatchdog.Status.PINGING and self._signaler is not None :
					self._signaler.add_pins ([self._pin],[1],
									count=self._timer, timed=True, source='NetworkWatchdog')
			time.sleep (delay)

	def status (self, *args, **kwargs) -> dict:
		""" pytel IStatus method fills a status dictionary """
		d = {}
		d['status']       = self._status
		d['uris']         = str(self._uris)
		d['timer']        = self._timer
		d['min_pingable'] = self._minping
		d['waits']        = self._waits
		d['ping_count']   = self._ping_count
		d['pin']          = self._pin
		return d

	def _check_network (self) :
		nerr = False
		loss = 0
		nuri = len(self._uris)
		for ip in self._uris :
			l = self.losses (ip)
			log.debug ('network watchdog loss to {0} is {1:.1f}%'.format(ip,l))
			loss += int(100*l)
		loss /= nuri

	def _losses (self, ip) :
		"""
		Test what percent loss occurs if a particular IP address is pinged.
		"""
		loss = 100.	# PERCENT
		stat,result = subprocess.getstatusoutput (self._ping_cmd)
		if stat != 0 :
			log.critical ('network watchdog cannot perform system ping! (status={0})'.format(stat))

		# FIND AMOUNT OF LOSS IN ping OUTPUT STRING

		for thing in result.split(',') :
			if '%' in thing :
				try :
					pos = thing.index('%')
					loss = float(thing[:pos])
				except ValueError :
					log.critical ('network watchdog cannot parse'+thing)
					loss = 100.
		return loss

	def _ping (self) :
			signal = False
			ok = self._check_network ()
			with self._watchdog_lock :
				if ok :
					self._status = NetworkWatchdog.Status.PINGING
					if self._signaler is not None :
						self._signaler.cancel_error (NetworkWatchdog.Status.NO_NETWORK)
					pings = self._ping_count
					pings += 1
					if pings >= self._waits :
						self._ping_count = 0
						signal = True
					else :
						self._ping_count = pings
				else :
					self._status = NetworkWatchdog.Status.NO_NETWORK
					if self._signaler is not None :
						self._signaler.add_error (NetworkWatchdog.Status.NO_NETWORK)

			# tell signaler to signal a complete ping cycle (outside of lock!)
			if signal and self._signaler is not None :
				self._signaler.signal (self,[1],self._role)

if __name__ == '__main__' :
	import time
	from ttlio import RPiIO
	print ('Testing NetworkWatcher...')

	print ('Starting GPIO thread...')
	io = RPiIO()

	print ('Starting network watching thread...')
	conf = {}
	conf['uris'] = ['www.google.com','www.gwdg.de']
	watcher = NetworkWatchdog(conf)

	c = input ('Type something to stop.')
