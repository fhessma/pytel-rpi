# am2302.py

"""
pytel driver for an AM2302 temperature+humidity sensor.
"""

import datetime
import logging
import os
import subprocess
import sys
import threading

from pytel_rpi.utils import pin2bcm

try :
	import Adafruit_DHT as ws
	def sensor_reading (pin) :
		"""
		Comments from the original Adafruit example:

		Try to grab a sensor reading.  Use the read_retry method which
		will retry up to 15 times to get a sensor reading (waiting 2
		seconds between each retry).  Note that sometimes you won't get
		a reading and the results will be null (because Linux can't
		guarantee the timing of calls to read the sensor).  If this
		happens try again!
		"""
		return ws.read_retry('2302', pin)

except ImportError :
	import am2302_rpi as ws
	def sensor_reading (pin) :
		bcm = pin2bcm (pin)
		t = ws.get_temperature (bcm)
		h = ws.get_humidty (bcm)
		return h,t

# log = logging.getLogger ('pytel')

class AM2302Sensor (object) :
	"""
	Object for reading an AM2302/DHT22 temperature+humidity sensor.
	A thread constantly monitors the humidity and sends the client a
	message if it is over the configured limit.
	"""
	def __init__ (self, warn_client=None, unwarn_client=None, \
						conf=None, *args, **kwargs) :
		self._warn   = warn_client
		self._unwarn = unwarn_client
		self._events = 0
		self.utc     = None
		self.temp    = None
		self.humid   = None

		# DEFAULT CONFIGURATION
		self._pin     = 4	# GPIO PIN
		self._sleep   = 10.	# SLEEP TIME IN SECONDS
		self.limit    = 90.	# % HUMIDITY
		self._nevents = 3	# NUMBER OF CONSECUTIVE EVENTS TO PRODUCE MESSAGE

		# OPTIONAL CONFIGURATION
		if conf is not None :
			try :
				if 'pins' in conf :
					if len(conf['pins']) == 1 :
						self._pin = conf['pins'][0]
			except :
				pass
			try :
				if 'sleep' in conf :
					self._sleep = float(conf['sleep'])
			except :
				pass
			try :
				if 'limit' in conf :
					self._limit = float(conf['limit'])
			except :
				pass
			try :
				if 'nevents' in conf :
					self._nevents = int(conf['nevents'])
			except :
				pass

		# SET UP THREAD AND START
		self._lock   = threading.Lock()
		self._thread = threading.Thread (target=self._measure)
		self._thread.setDaemon (True)
		self._thread.start()

	def _measure (self) :
		"""
		Method called by the thread to measure and process the
		temperature and humidity. In order to obtain a robust meaasurement,
		the limit triggering (up or down) has to occur at least self._nevents
		times before the client's warning or unwarning methods are called.
		"""
		attempts = 0
		while True :
			if self._lock.acquire(blocking=True) :
				t,h = sensor_reading(self._pin)
				if t is not None and h is not None :
					self.temp  = t
					self.humid = h
					self.utc = datetime.datetime.now()
					print (self.utc,t,h,self._events)

					if h >= self._limit :
						if self._events < self._nevents :
							self._events += 1
						if self._events >= self._nevents :
							self._warn()
					else :
						if self._events > -self._nevents :
							self._events -= 1
						if self._events <= -self._nevents :
							self._unwarn()
				self._lock.release()
				attempts = 0
			else :
				attempts += 1
				if attempts > 10 :
					logging.error ('cannot acquire AM2302 sensor thread!')
					attemtps = 0
			time.sleep (self._sleep)

if __name__ == '__main__' :
	def problem () :
		print (datetime.now(),' : humidity > limit!')
	def no_problem () :
		print (datetime.now(),' : humidity < limit!')
	sensor = AM2302Sensor (problem,no_problem)
	c = input ('**** Type <RET> to stop. ****\n')
