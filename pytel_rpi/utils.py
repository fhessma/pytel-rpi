# pytel_rpi/utils.py

# GPIO PIN TO BCM NUMBER DICTIONARY
p2b = {	 3:2,   5:3,   7:4,   8:14, 10:15,
		11:17, 12:18, 13:27, 15:22, 16:23,
		18:24, 19:10, 21:9,  22:25, 23:11,
		24:8,  26:7,  27:0,  28:1,  29:5,
		31:6,  32:12, 33:13, 35:19, 36:16,
		37:26, 38:20, 40:21 }

# VICE VERSA DICTIONARY
b2p = { v:k for k,v in p2b.items() }

def pin2bcm (pin) :
	"""
	Converts a GPIO pin number to a BCM number.
	"""
	if pin in p2b :
		return p2b[pin]
	else :
		None

def bcm2pin (bcm) :
	"""
	Converts a BCM number to a GPIO pin.
	"""
	if bcm in b2p :
		return b2p[bcm]
	else :
		None
