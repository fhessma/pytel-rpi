# serialio.py

"""
pytel_rpi module for operating a Raspberry Pi as a general-purpose controller
with serial communication.
"""

import logging
import serial
import sys
import threading

try :
	from gobject               import timeout_add
except ImportError :
	from gi.repository.GObject import timeout_add

log = logging.getLogger('pytel')

class RPiSerial (object):
	def __init__(self, info, *args, **kwargs):
		if 'buffersize' in info :
			self._serial_buffersize = info['buffersize']
		else :
			self._serial_buffersize = 512
		self._serial_buffer = None
		self._serial_info   = info

		device   = info['device']
		baudrate = info['baudrate']
		bits     = info['bits']
		stopbit  = info['stopbit']
		try :
			timer = int(info['timer'])*1000
		except ValueError :
			timer = 2000
		self._serial = serial.Serial (device,baudrate)
		self._serial_lock = threading.Lock()

		timeout_add (timer, self._get_serial_data)	# CHECK SERIAL INPUT EVERY 2 SECONDS

	def serial_status (self, *args, **kwargs):
		return self._serial_info

	def read_serial (self, nbytes) -> str :
		result = ''
		if self._serial is None :
			return result
		b = self._serial.read(1)
		while str(b) != self._prefix :
			b = self._serial.read(1)
		result += str(b)
		for i in range(nbytes-1) :
			b = self._serial.read(1)
			result += str(b)
		return result

	def write_serial (self, bytes) :
		self._serial.write(bytes)

	def _get_serial_input (self) :
		""" Reads current input and adds to the buffer. """
		if self._serial is None : return
		with self._serial_lock :
			# get new input
			result = ''
			b = self._serial.read(1)
			while str(b) != self._prefix :
				b = self._serial.read(1)
			result += str(b)
			for i in range(nbytes-1) :
				b = self._serial.read(1)
				result += str(b)
			l = len(result)
