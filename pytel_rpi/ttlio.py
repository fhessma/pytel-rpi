# ttlio.py

"""
Operating a Raspberry Pi as a general-purpose (slow) TTL-level I/O controller
"""

import datetime
import logging
import sys
import time
from threading import Lock,Thread

import RPi.GPIO

log = logging.getLogger('pytel')

class RPiIO (object):
	"""
	Object to operate the GPIO TTL ports of a Raspberry Pi.
	Every 1 second, the object checks to see if a timed port needs to be turned off.
	"""
	def __init__(self, errorpin=27, *args, **kwargs):
		self._gpio_pins = {}

		self._errors = {}
		self._error_pin = errorpin

		# CHECK PINS EVERY 1 SECOND
		log.info ('starting GPIO thread ...')
		self._gpio_lock = Lock()
		self._thread_stop = False
		self._thread = Thread (target=self._keep_checking)
		self._thread.setDaemon (True)
		self._thread.start ()

	def _keep_checking (self,delay=1) :
		""" Defines the pin checking thread. """
		while True :
			self._check_gpio_pins ()
			time.sleep (delay)

	def add_error (self, error_string) :
		""" Note a GPIO error and turn on error LED. """
		self._errors[error_string] = str(datetime.datetime.now())
		self._error_LED ()

	def cancel_error (self, error_string) :
		""" Un-note a GPIO error and turn off error LED if no more errors. """
		if error_string in self._errors :
			del self._errors[error_string]
			self._error_LED ()

	def _error_LED (self) :
		""" Operate the error LED. """
		if len(self._errors) > 0 :
			self._set_gpio_pins ([self._error_pin],[1])
		else :
			self._set_gpio_pins ([self._error_pin],[0])

	def gpio_init (self) :
		""" Turns off all GPIO pins in the pin dictionary. """
		with self._gpio_lock :
			for pin in self._gpio_pins :		# SEPARATE LOOP FOR SPEED
				self._set_gpio_pins ([pin],[0])
			self._errors = {}
			self._error_LED ()

	def current_pins (self) :
		""" Returns a dictionary of the currently used pins. """
		d = {}
		for pin,info in self._gpio_pins.items() :
			d[pin] = info['source']
		return d

	def _valid_pin_number (self, pin) :
		""" Checks for a valid BCM pin number. """
		# NEED TO CHECK FOR VALID PIN NUMBER HERE!!!!
		return True

	def add_pins (self, pinlist, vallist, count=0, source='UNKNOWN') -> bool :
		"""
		Adds a set of pins to the dictionary via their individual pin numbers, values, and their
		common time count if timed.
		"""
		onpins = []
		onvals = []
		ok = True
		with self._gpio_lock :
			try :
				for pin,val in zip(pinlist,vallist) :
					if not self._add_pin (pin,val,count,count > 0,source) :
						log.error ('could not add GPIO pin {0}'.format(pin))
						raise Exception('pin error')
					onpins.append(pin)
					onvals.append(val)
				self._set_gpio_pins (onpins,onvals)		# LISTS USED TO MINIMIZE DELAYS
			except :
				ok = False
		if not ok :
			self.remove_pins (pinlist)					# UN-DO ADDITION OF PINS (OUTSIDE OF LOCK)
		return ok

	def remove_pins (self, pinlist) -> bool:
		""" High-level removal of pins from management. """
		with self._gpio_lock :
			for pin in pinlist :
				self._remove_pin (pin)
		return True

	def _add_pin (self, pin, val, count, timed, source) -> bool:
		"""
		Low level addition of a GPIO pin. Does not actually turn pin ON/OFF!!!
		"""
		if not self._valid_pin_number (pin) :
			log.error ('{0} is not a valid GPIO BCM pin number!'.format(pin))
			return False
		self._gpio_pins[pin] = {'value':val,'count':count,'timed':timed,'source':source}
		return True

	def _remove_pin (self, pin) -> bool:
		""" Low-level removal of a GPIO pin from management. """
		if pin in self._gpio_pins :
			self._set_gpio_pins ([pin],[0])
			del self._gpio_pins[pin]
			return True
		else :
			return False

	def _set_gpio_pins (self, pinlist,vallist) :
		""" Low-level TTL setting of a list of RPi GPIO pins to a list of values. """
		RPi.GPIO.setwarnings (False)
		RPi.GPIO.setmode (RPi.GPIO.BCM)
		for i in range(len(pinlist)) :
			RPi.GPIO.setup (pinlist[i],RPi.GPIO.OUT)
			RPi.GPIO.output (pinlist[i],vallist[i])

	def _check_gpio_pins (self) :
		"""
		Updates the GPIO pin states and turns off any timed pins.
		This is used in the timed thread to maintain info and control.
		"""
		offpins = []
		offvals = []
		print ('Checking...')
		with self._gpio_lock :
			for pin in self._gpio_pins :
				info = self._gpio_pins[pin]
				val   = info['value']
				count = info['count']
				timed = info['timed']
				if timed and count > 0 :
					count -= 1
					if count == 0 :
						info['value'] = 0
						info['count'] = 0
						offpins.append(pin)
						offvals.append(0)
					else :
						info['count'] = count
			self._set_gpio_pins (offpins,offvals)		# LISTS USED TO MINIMIZE DELAYS

	def _blink (self,pin=17,duration=1) :
		""" Handy test method. """
		io._set_gpio_pins ([pin],[1])
		time.sleep (duration)
		io._set_gpio_pins ([pin],[0])

if __name__ == '__main__' :
	import time
	print ('Initialize GPIO...')
	io = RPiIO ()

	"""
	print ('Adding pins 16,17,22 ...')
	io.add_pins ([16,17,22], [1,1,1], 10, source='test1')
	time.sleep (3)
	print ('Adding pin 23...')
	io.add_pins ([23], [1], 10,source='test2')
	time.sleep (3)
	print ('Invoking error...')
	time.sleep (10)
	print ('Clearing pins...')
	io.gpio_init ()
	"""

	print ('Adding errors...')
	error_message = 'an unforgivable error'
	io.add_error (error_message)
	time.sleep (10)
	io.cancel_error (error_message)
	time.sleep (5)

	io.gpio_init ()
