# rpidriver.py

"""
pytel module for operating a Raspberry Pi as a general-purpose controller with GPIO I/O and a serial connection.

A typical configuration file might be something like

	serial :
		device : /dev/ttyUSB0
		baudrate : 9600
		bits : 8
		parity : N
		stopbit : 1

	clients :
		roof :
			gpio :
				ports : 17,19,20
				output: True
				timer : 5
			serial :
				input : True
		watchdog :
			gpio :
				ports : 21
				output: True
				timer : 10
		humidity :
			gpio :
				ports : 22
				input : True
"""

import logging

import serial
import rpi.gpio

from pytel.modules.module import PytelModule
from pytel.interfaces import IStatus, IAbort

log = logging.getLogger('pytel')


class RPi(PytelModule, IStatus.IStatus, IAbort.IAbort):
	def __init__(self, *args, **kwargs):
		PytelModule.__init__(self, *args, **kwargs)

		# open serial connection
		self._serial = None
		self._prefix = None
		if 'serial' in self.config :
			info = self.config['serial_port']
			device    = info['device']
			baudrate  = info['baudrate']
			bits      = info['bits']
			stopbit   = info['stopbit']
			if 'prefix' in info :
				self._prefix = info['prefix']
			self._serial = serial.Serial (device,baudrate)

		# configure clients
		self._clients = None
		if 'gpio' in self.config :
			self._clients = []
			for clientname in self.config['clients'] :
				info = self.config['clients'][clientname]
				cereal = {'input':False,'output':False}
				gpio = {'ports':None,'input':False,'output':False,'timer':None}
				if 'serial' in info :
					for thing in info['serial'] :
						if thing in cereal :
							cereal[thing] = info[thing]
					else :
						log.error ('Unknown GPIO configuration key: {0}'.format(thing))

	def close(self):
		if self._serial is not None :
			log.info('Closing serial connection...')
			self._serial.close()

	def status (self, *args, **kwargs):
		info = self.config['serial_port']
		stat = {}
		if 'serial' in self.config and self._serial is not None :
			stat['device']    = info['device']
			stat['baudrate']  = info['baudrate']
			stat['bits']      = info['bits']

	# ---- RPi METHODS

	def register (self, role, client, *args, **kwargs) :
		"""
		Method for registering clients for serial and GPIO I/O.
		Each client has a role string (e.g. "watchdog") with which the access rights defined
			in the configuration YAML are fixed.
		"""

	def read_serial (self, nbytes) -> str :
		result = ''
		if self._serial is None :
			return result
		b = self._serial.read(1)
		while str(b) != self._prefix :
			b = self._serial.read(1)
		result += str(b)
		for i in range(nbytes-1) :
			b = self._serial.read(1)
			result += str(b)
		return result

	def write_serial (self, bytes) :
		self._serial.write(bytes)

class WatchdogTimer(PytelModule):
	def __init__(self, *args, **kwargs):
		PytelModule.__init__(self, *args, **kwargs)

