# serialio.py

"""
pytel_rpi module for operating a Raspberry Pi as a general-purpose controller
with serial communication.
"""

import logging
import serial
import sys
import threading

# log = logging.getLogger('pytel')

class RPiSerial (object):
	def __init__(self, conf=None, *args, **kwargs):
		if 'buffersize' in info :
			self._serial_buffersize = info['buffersize']
		else :
			self._serial_buffersize = 512
		self._serial_buffer = None
		self._serial_info   = info
		self._prefix        = None
		self._suffix        = None

		self._device = '/dev/ttyUSB0'
		self._baudrate = 9600
		self._bits     = 8
		self._stopbit  = 1
		self._parity   = 'N'

		if conf is not None :
			try :
				if 'device' in conf :
					self._device   = conf['device']
				if 'baudrate' in conf :
					self._baudrate = int(conf['baudrate'])
				if 'bits' in conf :
					self._bits     = int(conf['bits'])
				if 'stopbit' in conf :
					self._stopbit  = int(conf['stopbit'])
				if 'parity' in conf :
					self._parity   = conf['parity']
				if 'prefix' in conf :
					self._prefix   = conf['prefix']
				if 'suffix' in conf :
					self._suffix   = conf['suffix']
			except ValueError :
				logging.critical ('serial port configuration error!')
				sys.exit (1)
		self.init_serial ()
		self._serial_lock = threading.Lock()

	def init_serial (self) :
		self._serial = serial.Serial (self._device,self._baudrate)

	def close_serial (self) :
		self._serial.close()

	def serial_status (self, *args, **kwargs):
		return self._serial_info

	def write_serial (self, bytes) :
		self._serial.write(bytes)

	def read_serial (self, nbytes) -> str :
		result = ''
		b = self._serial.read(1)
		while str(b) != self._prefix :
			b = self._serial.read(1)
		result += str(b)
		for i in range(nbytes-1) :
			b = self._serial.read(1)
			result += str(b)
		return result

	def _get_serial_input (self) :
		""" Reads current input and adds to the buffer. """
		with self._serial_lock :
			# get new input
			result = ''
			b = self._serial.read(1)
			while str(b) != self._prefix :
				b = self._serial.read(1)
			result += str(b)
			for i in range(nbytes-1) :
				b = self._serial.read(1)
				result += str(b)
			l = len(result)

if __name__ == '__main__' :
	ser = RPiSerial ()
	stuff = ['A','B','C']
	for c in stuff :
		ser.write (c)
		print (ser.read ())
	ser.close_serial ()
