# pytel-rpi.pytel_rpi.watch.py

"""
A network watchdog.  The Linux ping command

	/bin/ping -c 1 -W 5 www.gwdg.de

returns something like

	PING www-portal.gwdg.de (134.76.9.36) 56(84) bytes of data.
	64 bytes from www-portal.gwdg.de (134.76.9.36): icmp_seq=1 ttl=61 time=0.911 ms

	--- www-portal.gwdg.de ping statistics ---
	1 packets transmitted, 1 received, 0% packet loss, time 0ms
	rtt min/avg/max/mdev = 0.911/0.911/0.911/0.000 ms

from which the "*% packet loss" needs to be found and parsed.
"""

import logging
import os
import subprocess
import sys
import threading

from enum import Enum

from pytel.interfaces.IStatus import IStatus

log = logging.getLogger ('pytel')

class NetworkWatchdog (IStatus) :
	"""
	Pings a list of URI's and tells the RPI GPIO object to set a watchdog output signal if
	enough of the URI's are reachable.
	"""
	class Status(Enum) :
		ERROR      = 'error'
		NO_NETWORK = 'no network'
		PINGING    = 'pinging'
		UNKNOWN    = 'unknown'

	def __init__ (self, signaler, conf=None, *args, **kwargs) :
		""" Initialize using a given time [secs] and a list of URI's. """

		# DEFAULT CONFIGURATION

		self._pin          = 23		# GPIO PIN
		self._ping_sleep   = 5.		# SLEEP TIME FOR PING THREAD [SECS]
		self._sig_sleep    = 20.	# TIME BETWEEN SIGNALS [SECS]
		self._sig_duration = 5.		# DURATION OF A SINGLE SIGNAL [SECS]
		self._minping      = 1
		self._waits        = 3
		self._ping_count = 0
		self._max_loss   = 50.001	# PERCENT, I.E. AT LEAST 1 FROM 2

		self._status     = NetworkWatchdog.Status.UNKNOWN
		self._ping_cmd   = '/bin/ping -c 1 -W 5 {0}'
		self._uris       = ['www.google.com','www.gwdg.de']

		# OPTIONAL CONFIGURATION

		if conf is not None :
			if 'uris' in conf :
				self._uris = conf['uris']
			if 'ping_cmd' in conf :
				self._ping_cmd = conf['ping_cmd']
			if 'ping_sleep' in conf :
				self._timer = float(conf['ping_sleep'])
			if 'signal_sleep' in conf :
				self._sig_sleep = float(conf['signal_sleep'])
			if 'signal_duration' in conf :
				self._sig_duration = float(conf['signal_duration'])
			if 'max_loss' in conf :
				self._max_loss = conf['max_loss']
			if 'min_pingable' in conf :
				self._minping = conf['min_pingable']
			if 'waits' in conf :
				self._waits = conf['waits']
			if 'pins' in conf :
				if len(conf['pins']) == 1 :
					self._pin = conf['pins'][0]

		# SET UP THREADS

		if signaler is None :
			logging.critical ('network watchdog "signaler" not given - abort!')
			sys.exit(1)
		else :
			self._signaler = signaler
		
		self._watchdog_lock = threading.Lock()

		# START THE PINGING THREAD
		self._ping_thread = threading.Thread (target=self._start_pinging)
		self._ping_thread.setDaemon (True)
		self._ping_thread.start()

		# START THE SIGNALING THREAD
		self._signal_thread = threading.Thread (target=self._start_signaling)
		self._signal_thread.setDaemon (True)
		self._signal_thread.start()

	def set_signaler (self, signaler) :
		""" Assigns GPIO signalling object. """
		with self._watchdog_lock :
			self._signaler = signaler

	def _start_pinging (self) :
		""" The thread defined by this method should perform the URI searches every "timer" seconds. """
		while True :
			self._ping ()
			time.sleep (self._ping_sleep)

	def _start_signaling (self) :
		""" The thread defined by this method should perform the URI searches every "timer" seconds. """
		while True :
			with self._watchdog_lock :
				if self._status == NetworkWatchdog.Status.PINGING :
					self._signaler.add_pins ([self._pin],[1],
									count=self._sig_duration, source='NetworkWatchdog')
			time.sleep (self._sig_sleep)

	def status (self, *args, **kwargs) -> dict:
		""" pytel IStatus method fills a status dictionary """
		d = {}
		d['status']          = self._status
		d['uris']            = str(self._uris)
		d['ping_cmd']        = self._ping_cmd
		d['ping_sleep']      = self._ping_sleep
		d['signal_sleep']    = self._signal_sleep
		d['signal_duration'] = self._sig_duration
		d['max_loss']        = self._max_loss
		d['min_pingable']    = self._minping
		d['waits']           = self._waits
		d['pins']            = [self._pin]
		return d

	def _check_network (self) :
		nerr = False
		loss = 0.
		nuri = len(self._uris)
		for ip in self._uris :
			l = self._percent_loss (ip)
			logging.debug ('network watchdog loss to {0} is {1:.1f}%'.format(ip,l))
			loss += l
		loss /= nuri
		return loss < self._max_loss

	def _percent_loss (self, ip) :
		"""
		Test what percent loss occurs if a particular IP address is pinged.
		"""
		loss = 100.	# PERCENT
		stat,result = subprocess.getstatusoutput (self._ping_cmd.format(ip))
		if stat != 0 :
			logging.critical ('network watchdog cannot perform {0}! (status={1})'.format(self._ping_command,stat))

		# FIND AMOUNT OF LOSS IN ping OUTPUT STRING

		for thing in result.split(',') :
			if 'packet loss' in thing :
				try :
					pos = thing.index('%')
					loss = float(thing[:pos])
				except ValueError :
					logging.critical ('network watchdog cannot parse'+thing)
					loss = 100.
		return loss

	def _ping (self) :
		"""
		This is the ping test performed by the pinging thread.  The acutal GPIO
		signalling is done by the _start_pinging() loop based on the status set here.
		"""
		next_signal = False
		ok = self._check_network ()
		with self._watchdog_lock :
			if ok :
				self._status = NetworkWatchdog.Status.PINGING
				self._signaler.cancel_error (NetworkWatchdog.Status.NO_NETWORK)
				pings = self._ping_count
				pings += 1
				if pings >= self._waits :
					self._ping_count = 0
					next_signal = True
				else :
					self._ping_count = pings
			else :
				self._status = NetworkWatchdog.Status.NO_NETWORK
				self._signaler.add_error (NetworkWatchdog.Status.NO_NETWORK)

if __name__ == '__main__' :
	import time
	from ttlio import RPiIO
	print ('Testing NetworkWatcher...')

	print ('Starting GPIO thread...')
	io = RPiIO()
	io._blink (pin=23)

	print ('Starting network watching thread...')
	conf = {}
	conf['signal_sleep']    = 10
	conf['signal_duration'] = 2
	watcher = NetworkWatchdog (io,conf=conf)

	c = input ('**** Type <RET> to stop. ****\n')
	io.gpio_init()
